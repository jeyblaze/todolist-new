var input = document.querySelector("input[type = 'text']");
var ul = document.querySelector("ul");
var spans = document.getElementsByTagName("span");

function deleteTodo(){
  for(let span of spans){
    span.addEventListener ("click",function (){
      span.parentElement.remove();
    });
  }
}

input.addEventListener("keypress",function(keyPressed){
  if(keyPressed.which === 13){
    var li = document.createElement("li");
    var spanElement = document.createElement("span");
    var icon = document.createElement("i");
    var newTodo = this.value;

    this.value = " " ;
    spanElement.append(icon);
    ul.appendChild(li).append(spanElement,newTodo);
    deleteTodo();
    }   
});

deleteTodo();
